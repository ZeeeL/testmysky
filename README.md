# USAGE

To install dependencies for OS X users, type:

    easy_install selenium webium nose
    brew install geckodriver

To run tests, go to directory and type:

    nosetests

To specify another url, type:

    env TEST_URL='https://client.mysky.dev' nosetests
