# -*- coding: utf-8 -*-

import unittest
from selenium.webdriver.common.keys import Keys
from webium.driver import get_driver
from pages import LoginPage, AccountPage

class TestLoginPage(unittest.TestCase):
    def setUp(self):
        login_page = LoginPage()
        login_page.open()

    def tearDown(self):
        get_driver().quit()

    @unittest.expectedFailure
    def test_success_login(self):
        login_page = LoginPage()

        login_page.login.send_keys(u'user1@gmail.com')
        login_page.password.send_keys(u'qwerty')
        login_page.submit.click()

        account_page = AccountPage()
        self.assertTrue(account_page.is_element_present('username'))
        self.assertEqual(account_page.username.text, 'user1')

    @unittest.expectedFailure
    def test_empty_login_form(self):
        login_page = LoginPage()

        self.assertFalse(login_page.submit.is_enabled())

    @unittest.expectedFailure
    def test_fill_login_form_only_with_email(self):
        login_page = LoginPage()

        login_page.login.send_keys(u'user1@gmail.com')
        self.assertFalse(login_page.submit.is_enabled())

    @unittest.expectedFailure
    def test_fill_login_form_only_with_password(self):
        login_page = LoginPage()

        login_page.password.send_keys(u'qwerty')
        self.assertFalse(login_page.submit.is_enabled())

    @unittest.expectedFailure
    def test_fill_login_form_with_wrong_email(self):
        login_page = LoginPage()

        login_page.login.send_keys(u'wrong_user@gmail.com')
        login_page.password.send_keys(u'qwerty')
        login_page.submit.click()

        self.assertTrue(login_page.is_element_present('error'))
        self.assertEqual(login_page.error.text, 'Wrong email')

    @unittest.expectedFailure
    def test_fill_login_form_with_wrong_password(self):
        login_page = LoginPage()
        login_page.login.send_keys(u'user1@gmail.com')
        login_page.password.send_keys(u'wrong_password')
        login_page.submit.click()

        self.assertTrue(login_page.is_element_present('error'))
        self.assertEqual(login_page.error.text, 'Wrong password')
