# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By
from webium import BasePage, Find
from utils import get_page_url


class LoginPage(BasePage):
    url = get_page_url('/login')

    login = Find(by=By.NAME, value='login')
    password = Find(by=By.NAME, value='password')
    submit = Find(by=By.XPATH, value="//button[@type='submit']")

    error = Find(by=By.NAME, value='error')
