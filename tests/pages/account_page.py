# -*- coding: utf-8 -*-

from selenium.webdriver.common.by import By
from webium import BasePage, Find
from utils import get_page_url


class AccountPage(BasePage):
    url = get_page_url('/me')

    username = Find(by=By.NAME, value='username')
