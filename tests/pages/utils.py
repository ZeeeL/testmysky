# -*- coding: utf-8 -*-

import os
from urlparse import urljoin

def get_page_url(path):
	return urljoin(
		os.getenv('TEST_URL', 'https://client.mysky.com'),
		path
	)
